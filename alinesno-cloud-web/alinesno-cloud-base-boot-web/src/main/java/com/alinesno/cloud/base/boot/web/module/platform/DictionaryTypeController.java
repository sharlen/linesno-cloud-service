package com.alinesno.cloud.base.boot.web.module.platform;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.feign.dto.ManagerCodeTypeDto;
import com.alinesno.cloud.base.boot.feign.facade.ManagerCodeTypeFeigin;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.bean.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;
import com.alinesno.cloud.common.web.base.form.FormToken;
import com.alinesno.cloud.common.web.base.response.ResponseBean;
import com.alinesno.cloud.common.web.base.response.ResponseGenerator;

/**
 * 字典控制层
 * @author LuoAnDong
 * @since 2018年11月27日 上午6:41:40
 */
@Controller
@RequestMapping("boot/platform/dictionaryType")
@Scope(SpringInstanceScope.PROTOTYPE)
public class DictionaryTypeController extends FeignMethodController<ManagerCodeTypeDto , ManagerCodeTypeFeigin>  {

	private static final Logger log = LoggerFactory.getLogger(DictionaryTypeController.class) ; 

	/**
	 * 代码管理查询功能  
	 * @return
	 */
	@GetMapping("/list")
    public void list(){
    }

	@TranslateCode(value="[{hasStatus:has_status}]", plugin="dictionaryTypeTranslatePlugin")
	@ResponseBody
	@RequestMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, feign , page) ;
    }
	
	
	/**
	 * 保存新对象 
	 * @param model
	 * @param managerCodeDto
	 * @return
	 */
	@FormToken(remove=true)
	@ResponseBody
	@PostMapping("/saveType")
	public ResponseBean saveType(Model model , HttpServletRequest request, ManagerCodeTypeDto managerCodeTypeDto) {
		
		managerCodeTypeDto = feign.save(managerCodeTypeDto) ; 
		
		return ResponseGenerator.ok(null) ; 	
	}
	
	/**
	 * 代码管理查询功能  
	 * @return
	 */
	@FormToken(save=true)
	@GetMapping("/addType")
    public void addType(Model model , HttpServletRequest request){
    }
	
	/**
	 * 代码管理查询功能  
	 * @return
	 */
	@FormToken(save=true)
	@GetMapping("/modifyType")
    public void modifyType(Model model , String id){
		Assert.hasLength(id , "主键不能为空.");
		
		ManagerCodeTypeDto code = feign.getOne(id) ; 
	
		model.addAttribute("bean", code) ; 
    }
	
	/**
	 * 保存
	 * @param model
	 * @param request
	 * @param managerApplicationDto
	 * @return
	 */
	@ResponseBody
	@PostMapping("/updateType")
	public ResponseBean updateType(Model model , HttpServletRequest request, ManagerCodeTypeDto managerCodeTypeDto) {
		
		ManagerCodeTypeDto oldBean = feign.getOne(managerCodeTypeDto.getId()) ; 
		
		BeanUtils.copyProperties(managerCodeTypeDto, oldBean);
		
		managerCodeTypeDto = feign.save(oldBean) ; 
		return ResponseGenerator.ok(null) ; 	
	}
	
	/**
	 * 删除
	 */
	@ResponseBody
	@PostMapping("/deleteType")
    public ResponseBean deleteType(@RequestParam(value = "rowsId[]") String[] rowsId){
		log.debug("rowsId = {}" , ToStringBuilder.reflectionToString(rowsId));
		if(rowsId != null && rowsId.length > 0) {
			feign.deleteByIds(rowsId); 
		}
		return ResponseGenerator.ok(null) ; 
    }
 
}
















