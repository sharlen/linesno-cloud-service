package com.alinesno.cloud.base.boot.web.plugin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alinesno.cloud.base.boot.feign.dto.ManagerCodeTypeDto;
import com.alinesno.cloud.base.boot.feign.facade.ManagerCodeTypeFeigin;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.advice.TranslatePlugin;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * 部门内容转换
 * @author LuoAnDong
 * @since 2019年4月7日 下午10:04:58
 */
@Component
public class DictionaryTypeTranslatePlugin implements TranslatePlugin {

	@Autowired
	private ManagerCodeTypeFeigin managerCodeTypeFeigin ; 

	private String CODETYPEVALUE = "codeTypeValue" ; 
	
	@Override
	public void translate(ObjectNode node, TranslateCode convertCode) {
		JsonNode codeTypeValue = node.get(CODETYPEVALUE) ; 
		
		String value = "" ; 
		if(!codeTypeValue.isNull()) {
			log.debug("codeTypeValue node = {}" , codeTypeValue.asText());
			
			ManagerCodeTypeDto dto = managerCodeTypeFeigin.findByCodeTypeValue(codeTypeValue.asText()) ; 
			if(dto != null) {
				value = dto.getCodeTypeName() ;
			}
		}
		node.put(CODETYPEVALUE + LABEL_SUFFER, value) ; 
	}

}
