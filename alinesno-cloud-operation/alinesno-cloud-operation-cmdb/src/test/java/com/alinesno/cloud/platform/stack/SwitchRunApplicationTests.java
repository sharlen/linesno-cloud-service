package com.alinesno.cloud.platform.stack;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SwitchRunApplicationTests {

	@Test
	public void contextLoads() {
		System.out.println("this is a test of test!");
	}

}
