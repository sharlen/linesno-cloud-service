package com.alinesno.cloud.platform.stack.sms;

import static org.junit.Assert.fail;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.third.sms.AliyunSmsEunms;
import com.alinesno.cloud.operation.cmdb.third.sms.SmsResponse;
import com.alinesno.cloud.operation.cmdb.third.sms.SmsSendService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmsSendServiceTest {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 
	String phone = "15578942583" ; 

	@Autowired
	private SmsSendService smsSendService ; 
	
	@Test
	public void testSendValidateSms() {
		SmsResponse response = smsSendService.sendOrderStatusSms(phone, "测试用户下单短信", "不用回复.") ;
		
		logger.info("response = {}" , ToStringBuilder.reflectionToString(response));
	}

	@Test
	public void testSendResetPasswordSms() {
		fail("Not yet implemented");
	}

	@Test
	public void testSendApplyPhoneSms() {
		SmsResponse response = smsSendService.sendApplyPhoneSms(phone) ; 
		Object code = response.getParams().get(AliyunSmsEunms.CODE.value()) ; 
		logger.info("response = {} , code = {}" , ToStringBuilder.reflectionToString(response) , code);
	}

	@Test
	public void testTemplateToContent() {
		fail("Not yet implemented");
	}

	@Test
	public void testSendOrderStatusSms() {
		fail("Not yet implemented");
	}

}
