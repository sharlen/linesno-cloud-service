package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 菜单资源表
 * @author LuoAnDong
 * @since 2018年9月23日 上午6:36:57
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_resources")
public class ResourceEntity extends BaseEntity implements Comparable<ResourceEntity>{
	
	@Override
	public String toString() {
		return "ResourceEntity [name=" + name + ", icons=" + icons + ", link=" + link + ", resourceSort=" + resourceSort
				+ "]";
	}
	private String name ; //名称 
	private String icons ; //图标 
	private String link ; //链接
	private Integer resourceSort ; //排序
	
	public ResourceEntity(Integer resourceSort , String name, String link, String icons) {
		this.resourceSort = resourceSort ; 
		this.name = name ; 
		this.link = link ; 
		this.icons = icons ; 
	}
	
	public ResourceEntity() {
		super();
	}

	public Integer getResourceSort() {
		return resourceSort;
	}
	public void setResourceSort(int resourceSort) {
		this.resourceSort = resourceSort;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIcons() {
		return icons;
	}
	public void setIcons(String icons) {
		this.icons = icons;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}

	@Override
	public int compareTo(ResourceEntity o) {
		return (o.getResourceSort()).compareTo(this.getResourceSort());
	}

}
