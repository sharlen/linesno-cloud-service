package com.alinesno.cloud.operation.cmdb.service.impl;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.operation.cmdb.entity.MasterMachineEntity;
import com.alinesno.cloud.operation.cmdb.repository.MasterMachineRepository;
import com.alinesno.cloud.operation.cmdb.service.MasterMachineService;
import com.alinesno.cloud.operation.cmdb.service.ParamsService;

/**
 * 机房数据操作
 * @author LuoAnDong
 * @since 2018年9月23日 下午8:46:57
 */
@Service
public class MasterMachineServiceImpl implements MasterMachineService {

	@Autowired
	private ParamsService paramsService ; 
	
	@Autowired
	private MasterMachineRepository masterMachineRepository ; 
	
	@Override
	public MasterMachineEntity findById(String id) {
		return masterMachineRepository.findById(id).get() ;
	}

	@Transactional
	@Override
	public boolean saveMasterMachine(MasterMachineEntity e , String masterCode) {
		
		e.setAddTime(new Date());
		masterMachineRepository.save(e) ;
		
		paramsService.initSchoolParam(masterCode); 
		
		return true ;
	}

	@Override
	public MasterMachineEntity findByMasterCode(String masterCode) {
		return masterMachineRepository.findByMasterCode(masterCode) ; 
	}

}
