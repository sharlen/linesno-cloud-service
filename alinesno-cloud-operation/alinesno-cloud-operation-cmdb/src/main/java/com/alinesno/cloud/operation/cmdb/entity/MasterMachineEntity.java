package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 机房信息实体对象
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午12:12:49
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_master_machine")
public class MasterMachineEntity extends BaseEntity {

	private String masterAddress; // 机房地址
	private String masterName; // 机房名称
	private String masterType; // 办学类型代码
	
	private String masterProvince; // 
	private String masterProvinceCode; // 
	private String masterTypeName; // 办学类型名称
	private String masterProperties; // 机房性质类型
	private String masterPropertiesCode; // 办学性质类型代码
	private String masterOwner; // 机房举办者
	private String masterOwnerCode; // 机房举办者代码
	private int masterOrder ; //排序
	
	public String getMasterAddress() {
		return masterAddress;
	}
	public void setMasterAddress(String masterAddress) {
		this.masterAddress = masterAddress;
	}
	public String getMasterName() {
		return masterName;
	}
	public void setMasterName(String masterName) {
		this.masterName = masterName;
	}
	public String getMasterType() {
		return masterType;
	}
	public void setMasterType(String masterType) {
		this.masterType = masterType;
	}
	public String getMasterProvince() {
		return masterProvince;
	}
	public void setMasterProvince(String masterProvince) {
		this.masterProvince = masterProvince;
	}
	public String getMasterProvinceCode() {
		return masterProvinceCode;
	}
	public void setMasterProvinceCode(String masterProvinceCode) {
		this.masterProvinceCode = masterProvinceCode;
	}
	public String getMasterTypeName() {
		return masterTypeName;
	}
	public void setMasterTypeName(String masterTypeName) {
		this.masterTypeName = masterTypeName;
	}
	public String getMasterProperties() {
		return masterProperties;
	}
	public void setMasterProperties(String masterProperties) {
		this.masterProperties = masterProperties;
	}
	public String getMasterPropertiesCode() {
		return masterPropertiesCode;
	}
	public void setMasterPropertiesCode(String masterPropertiesCode) {
		this.masterPropertiesCode = masterPropertiesCode;
	}
	public String getMasterOwner() {
		return masterOwner;
	}
	public void setMasterOwner(String masterOwner) {
		this.masterOwner = masterOwner;
	}
	public String getMasterOwnerCode() {
		return masterOwnerCode;
	}
	public void setMasterOwnerCode(String masterOwnerCode) {
		this.masterOwnerCode = masterOwnerCode;
	}
	public int getMasterOrder() {
		return masterOrder;
	}
	public void setMasterOrder(int masterOrder) {
		this.masterOrder = masterOrder;
	}

}
