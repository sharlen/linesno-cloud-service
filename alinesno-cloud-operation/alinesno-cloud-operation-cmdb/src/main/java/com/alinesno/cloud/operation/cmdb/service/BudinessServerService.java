package com.alinesno.cloud.operation.cmdb.service;

import com.alinesno.cloud.operation.cmdb.entity.ServerEntity;

public interface BudinessServerService {


	/**
	 * 通过机房查访服务
	 * @param masterCode
	 * @return
	 */
	Iterable<ServerEntity> findByMasterCode(String masterCode);

}
