package com.alinesno.cloud.operation.cmdb.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alibaba.fastjson.annotation.JSONField;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * 健康检查 
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午12:20:30
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_headth")
public class ResourceHeadthEntity extends BaseEntity {

	@Excel(name = "检查项")
	private String headthJob ;  // 检查项
	
	private String headthStatus ; //健康状态
	
	@Excel(name = "健康链接")
	private String headthLink ; //健康链接
	
	@Excel(name = "健康地址")
	private String tcpHost ; //健康地址
	
	@Excel(name = "健康端口")
	private String tcpPort; //健康端口

	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date lastHeadthTime ; // 最后检查时间 
	
	@Excel(name = "检查周期" , type=10)
	private Integer checkLoop ; // 检查周期(秒)
	
	@Excel(name = "检查类型")
	private String checkType ; // 检查类型(http/tcp)
	
	public void setLastHeadthTime(Date lastHeadthTime) {
		this.lastHeadthTime = lastHeadthTime;
	}
	public String getHeadthLink() {
		return headthLink;
	}
	public void setHeadthLink(String headthLink) {
		this.headthLink = headthLink;
	}
	public String getTcpHost() {
		return tcpHost;
	}
	public void setTcpHost(String tcpHost) {
		this.tcpHost = tcpHost;
	}
	public String getTcpPort() {
		return tcpPort;
	}
	public void setTcpPort(String tcpPort) {
		this.tcpPort = tcpPort;
	}
	public String getCheckType() {
		return checkType;
	}
	public void setCheckType(String checkType) {
		this.checkType = checkType;
	}
	public Integer getCheckLoop() {
		return checkLoop;
	}
	public void setCheckLoop(Integer checkLoop) {
		this.checkLoop = checkLoop;
	}
	public String getHeadthJob() {
		return headthJob;
	}
	public void setHeadthJob(String headthJob) {
		this.headthJob = headthJob;
	}
	public String getHeadthStatus() {
		return headthStatus;
	}
	public void setHeadthStatus(String headthStatus) {
		this.headthStatus = headthStatus;
	}
	public Date getLastHeadthTime() {
		return lastHeadthTime;
	}
	
}
