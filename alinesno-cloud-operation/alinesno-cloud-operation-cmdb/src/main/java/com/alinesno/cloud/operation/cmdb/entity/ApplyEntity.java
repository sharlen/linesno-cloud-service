package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 用户申请
 * @author LuoAnDong
 * @since 2018年8月13日 下午9:59:06
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_apply")
public class ApplyEntity extends BaseEntity {

	private String userId ; //申请用户
	private String applyManager ; //审批用户
	private String applyContent ; //审批意见 
	private String applyStatus ; //审批状态
	
	public String getApplyStatus() {
		return applyStatus;
	}
	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus;
	}
	public String getApplyContent() {
		return applyContent;
	}
	public void setApplyContent(String applyContent) {
		this.applyContent = applyContent;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getApplyManager() {
		return applyManager;
	}
	public void setApplyManager(String applyManager) {
		this.applyManager = applyManager;
	}
	
}
