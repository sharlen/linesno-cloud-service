package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * 网络ACL访问控制说明
 * @author LuoAnDong
 * @since 2019年5月17日 上午7:46:17
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_network_acl")
public class NetworkAclEntity extends BaseEntity {

	@Excel(name = "虚拟机名称")
	private String networkDevice;	// 网络安全设备
	
	@Excel(name = "源设备或应用类型")
	private String sourceDeviceName   ; // 源设备或应用类型,如桌面pc机
	
	@Excel(name = "源设备或应用名称")
	private String sourceApplicationName   ; // 源设备或应用名称,如门户网站
	
	@Excel(name = "业务名称")
	private String sourceAppBusinessName   ; // 业务名称,如门户网站【显示】
	
	@Excel(name = "到期时间")
	private String sourceEndTime   ; // 到期时间,如永久【显示】
	
	@Excel(name = "源虚拟机主机名称")
	private String sourceHostname   ; // 源虚拟机或主机名称，如个人web前端，【显示】
	
	@Excel(name = "源虚拟机IP地址")
	private String sourceHostip   ; // 源虚拟机或主机IP地址，如192.168.1.1，【显示】
	
	@Excel(name = "源虚拟机子网掩码")
	private String sourceHostMask   ; // 源虚拟机（VM or LPAR）或主机子网掩码，如:255.0.0.0
	
	@Excel(name = "源VLAN号码")
	private String sourceVlanNumber   ; // 源VLAN（PVLAN）号码
	
	@Excel(name = "源网络区域")
	private String sourceArea   ; // 源网络区域
	
	@Excel(name = "目标设备或应用名称")
	private String targetApplicationName   ; // 目标设备或应用名称，【显示】
	
	@Excel(name = "目标虚拟机主机名称")
	private String targetHostname   ; // 目标虚拟机（VM or LPAR）或主机名称
	
	@Excel(name = "目标虚拟机IP地址")
	private String targetHostip   ; // 目标虚拟机（VM or LPAR）或主机IP地址【显示】
	
	@Excel(name = "目标虚拟机子网掩码")
	private String targetHostMask   ; // 目标虚拟机（VM or LPAR）或主机子网掩码
	
	@Excel(name = "目标VLAN号码")
	private String targetVlanNumber   ; // 目标VLAN（PVLAN）号码
	
	@Excel(name = "目标网络区域")
	private String targetArea   ; // 目标网络区域
	
	@Excel(name = "目标虚拟机服务协议")
	private String targetNetworkProtocol   ; // 服务协议，如tcp，【显示】
	
	@Excel(name = "目标虚拟机开放端口")
	private String targetNetworkOpenPort   ; // 开放的端口号，如8080，【显示】
	
	@Excel(name = " 操作人员")
	private String operatorMan   ; // 操作人员

	public String getNetworkDevice() {
		return networkDevice;
	}

	public void setNetworkDevice(String networkDevice) {
		this.networkDevice = networkDevice;
	}

	public String getSourceDeviceName() {
		return sourceDeviceName;
	}

	public void setSourceDeviceName(String sourceDeviceName) {
		this.sourceDeviceName = sourceDeviceName;
	}

	public String getSourceApplicationName() {
		return sourceApplicationName;
	}

	public void setSourceApplicationName(String sourceApplicationName) {
		this.sourceApplicationName = sourceApplicationName;
	}

	public String getSourceAppBusinessName() {
		return sourceAppBusinessName;
	}

	public void setSourceAppBusinessName(String sourceAppBusinessName) {
		this.sourceAppBusinessName = sourceAppBusinessName;
	}

	public String getSourceEndTime() {
		return sourceEndTime;
	}

	public void setSourceEndTime(String sourceEndTime) {
		this.sourceEndTime = sourceEndTime;
	}

	public String getSourceHostname() {
		return sourceHostname;
	}

	public void setSourceHostname(String sourceHostname) {
		this.sourceHostname = sourceHostname;
	}

	public String getSourceHostip() {
		return sourceHostip;
	}

	public void setSourceHostip(String sourceHostip) {
		this.sourceHostip = sourceHostip;
	}

	public String getSourceHostMask() {
		return sourceHostMask;
	}

	public void setSourceHostMask(String sourceHostMask) {
		this.sourceHostMask = sourceHostMask;
	}

	public String getSourceVlanNumber() {
		return sourceVlanNumber;
	}

	public void setSourceVlanNumber(String sourceVlanNumber) {
		this.sourceVlanNumber = sourceVlanNumber;
	}

	public String getSourceArea() {
		return sourceArea;
	}

	public void setSourceArea(String sourceArea) {
		this.sourceArea = sourceArea;
	}

	public String getTargetApplicationName() {
		return targetApplicationName;
	}

	public void setTargetApplicationName(String targetApplicationName) {
		this.targetApplicationName = targetApplicationName;
	}

	public String getTargetHostname() {
		return targetHostname;
	}

	public void setTargetHostname(String targetHostname) {
		this.targetHostname = targetHostname;
	}

	public String getTargetHostip() {
		return targetHostip;
	}

	public void setTargetHostip(String targetHostip) {
		this.targetHostip = targetHostip;
	}

	public String getTargetHostMask() {
		return targetHostMask;
	}

	public void setTargetHostMask(String targetHostMask) {
		this.targetHostMask = targetHostMask;
	}

	public String getTargetVlanNumber() {
		return targetVlanNumber;
	}

	public void setTargetVlanNumber(String targetVlanNumber) {
		this.targetVlanNumber = targetVlanNumber;
	}

	public String getTargetArea() {
		return targetArea;
	}

	public void setTargetArea(String targetArea) {
		this.targetArea = targetArea;
	}

	public String getTargetNetworkProtocol() {
		return targetNetworkProtocol;
	}

	public void setTargetNetworkProtocol(String targetNetworkProtocol) {
		this.targetNetworkProtocol = targetNetworkProtocol;
	}

	public String getTargetNetworkOpenPort() {
		return targetNetworkOpenPort;
	}

	public void setTargetNetworkOpenPort(String targetNetworkOpenPort) {
		this.targetNetworkOpenPort = targetNetworkOpenPort;
	}

	public String getOperatorMan() {
		return operatorMan;
	}

	public void setOperatorMan(String operatorMan) {
		this.operatorMan = operatorMan;
	}

}
