package com.alinesno.cloud.operation.cmdb.common.constants;

/**
 * 订单状态
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午6:41:23
 */
public enum AccountStatusEnum {

	NORMARL("1" , "正常"), // 待接单
	UN_NORMAL("0" , "不正常"); // 已接单
	
	private String code;
	private String text ;
	
	AccountStatusEnum(String code , String text) {
		this.code = code;
		this.text = text ; 
	}
	
	public static AccountStatusEnum getStatus(String code) {
		if("1".equals(code)) {
			return NORMARL; 
		}else if("0".equals(code)) {
			return AccountStatusEnum.UN_NORMAL; 
		}
		return NORMARL ; 
	}
	
	public String getText() {
		return this.text ; 
	}

	public String getCode() {
		return this.code;
	}
}