package com.alinesno.cloud.operation.cmdb.service;

public interface LoggerService {

	/**
	 * 记录日志
	 */
	public void record(String userId , String type) ; 
	
}
