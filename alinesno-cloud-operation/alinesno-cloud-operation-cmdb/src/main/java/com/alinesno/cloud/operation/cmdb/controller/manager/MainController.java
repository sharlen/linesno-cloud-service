package com.alinesno.cloud.operation.cmdb.controller.manager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alinesno.cloud.operation.cmdb.controller.BaseController;
import com.alinesno.cloud.operation.cmdb.web.bean.ResponseBean;

/**
 * 后台主面板
 * 
 * @author LuoAnDong
 * @since 2018年8月14日 下午12:58:05
 */
@Controller
@RequestMapping("/manager/")
public class MainController extends BaseController {

	@SuppressWarnings("unused")
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * 后台主面板
	 * 
	 * @param model
	 * @param serverId
	 * @return
	 */
	@GetMapping("/main")
	public String main(Model model) {
		createMenus(model); 
	
		return WX_MANAGER + "main";
	}

	@GetMapping("/dashboard")
	public String dashboard(Model model) {
		createMenus(model); 
		return "dashboard/dashboard";
	}

	

	@RequestMapping("/footer")
	private String backgroud(ModelMap map) {
		map.addAttribute("headerContent", "hello word!");
		return WX_MANAGER+"footer";
	}

	/**
	 * 登陆验证
	 * 
	 * @return
	 */
	public ResponseBean<String> validate() {
		return null;
	}

}
