package com.alinesno.cloud.operation.cmdb.controller.manager;

import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.operation.cmdb.advice.ConvertCode;
import com.alinesno.cloud.operation.cmdb.controller.BaseController;
import com.alinesno.cloud.operation.cmdb.entity.MasterMachineEntity;
import com.alinesno.cloud.operation.cmdb.service.MasterMachineService;
import com.alinesno.cloud.operation.cmdb.web.bean.JqDatatablesPageBean;
import com.alinesno.cloud.operation.cmdb.web.bean.ResponseBean;
import com.alinesno.cloud.operation.cmdb.web.bean.ResultGenerator;

/**
 * 后台主面板
 * @author LuoAnDong
 * @since 2018年8月14日 下午12:58:05
 */
@Controller
@RequestMapping("/manager/")
public class ManagerMasterMachineController extends BaseController{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 
	
	@Autowired
	private MasterMachineService masterMachineService ; 
	
	@GetMapping("/master_add")
	public String masterAdd(Model model) {
		return WX_MANAGER+"master_add";
	}

	@GetMapping("/master_modify/{id}")
	public String masterModify(Model model , @PathVariable("id") String id) {
		logger.debug("id = {}" , id);
		
		MasterMachineEntity bean = masterMachineService.findById(id) ; 
		model.addAttribute("bean" , bean) ; 
		
		return WX_MANAGER+"master_modify";
	}
	
	/**
	 * 数据列表
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@ConvertCode
	@ResponseBody
	@GetMapping("/master_list_data")
	public JqDatatablesPageBean masterListData(Model model , JqDatatablesPageBean page) {
//		return this.toPage(model, masterRepository, page); 
		
		return this.toPage(model, 
				page.buildFilter(MasterMachineEntity.class , request)  , 
				masterMachineRepository, 
				page); 
	}
	
	
	@ResponseBody
	@PostMapping("/master_modify_save")
	public ResponseBean<String> modifySave(Model model , MasterMachineEntity e) {
		logger.debug("e = {}" , ToStringBuilder.reflectionToString(e));
		
		if(StringUtils.isBlank(e.getMasterCode())) {
			return ResultGenerator.genSuccessMessage("机房代码不能为空.");
		}

		MasterMachineEntity ee = masterMachineRepository.findById(e.getId()).get() ; 
		
//		ee.setSchoolName(e.getSchoolName());
//		ee.setSchoolOrder(e.getSchoolOrder());
		
		ee.setUpdateTime(new Date());
		BeanUtils.copyProperties(e, ee); 
		
		masterMachineRepository.save(ee) ; 
		
		return ResultGenerator.genSuccessMessage("保存成功");
	}
	 
	@ResponseBody
	@PostMapping("/master_save")
	public ResponseBean<String> save(Model model , MasterMachineEntity e) {
		logger.debug("e = {}" , ToStringBuilder.reflectionToString(e));
		
		// 判断机房是否存在 
		MasterMachineEntity master = masterMachineService.findByMasterCode(e.getMasterCode()) ; 
		if(master != null) {
			return ResultGenerator.genFailMessage("机房代码已存在.") ; 
		}
		
		boolean b = masterMachineService.saveMasterMachine(e , e.getMasterCode()) ; 
		
		return b?ResultGenerator.genSuccessMessage("保存成功"):ResultGenerator.genFailMessage("保存失败");
	}
	


}
