package com.alinesno.cloud.operation.cmdb.web.bean;

/**
 * 菜单实体对象 
 * @author LuoAnDong
 * @since 2018年8月15日 下午12:55:15
 */
public class MenuBean {

	private String id ; //id
	private String name ; //名称 
	private String icons ; //图标 
	private String link ; //链接
	
	public MenuBean(String id, String name, String link, String icons) {
		this.id = id ; 
		this.name = name ; 
		this.link = link  ; 
		this.icons = icons ; 
	}
	public String getIcons() {
		return icons;
	}
	public void setIcons(String icons) {
		this.icons = icons;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
