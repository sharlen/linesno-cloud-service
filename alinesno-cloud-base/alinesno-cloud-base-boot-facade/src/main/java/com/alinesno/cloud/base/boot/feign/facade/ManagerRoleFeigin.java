package com.alinesno.cloud.base.boot.feign.facade;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.alinesno.cloud.base.boot.feign.dto.ManagerAccountDto;
import com.alinesno.cloud.base.boot.feign.dto.ManagerRoleDto;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:02:27
 */
@FeignClient(name="alinesno-cloud-base-boot" , path="managerRole")
public interface ManagerRoleFeigin extends IBaseFeign<ManagerRoleDto> {
	
	/**
	 * 保存用户权限
	 * @param managerRoleDto
	 * @param functiondIds
	 * @return
	 */
	@PostMapping("saveRole")
	boolean saveRole(@RequestBody ManagerRoleDto managerRoleDto, @RequestParam("functionIds") String functionIds);

	/**
	 * 用户授权
	 * @param accountId
	 * @param rolesId
	 * @return
	 */
	@PostMapping("authAccount")
	boolean authAccount(@RequestBody ManagerAccountDto accountDot, @RequestParam("rolesId") String rolesId);

	/**
	 * 查询用户所有角色
	 * @param accountId
	 * @return
	 */
	@GetMapping("findByAccountId")
	List<ManagerRoleDto> findByAccountId(@RequestParam("accountId")  String accountId);

}
