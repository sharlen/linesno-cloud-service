package com.alinesno.cloud.base.boot.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.alinesno.cloud.base.boot.feign.dto.ManagerAccountDto;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:02:27
 */
@FeignClient(name="alinesno-cloud-base-boot" , path="managerAccount")
public interface ManagerAccountFeigin extends IBaseFeign<ManagerAccountDto> {

	// 根据登陆用户名获取密码
	@PostMapping("findByLoginName")
	ManagerAccountDto findByLoginName(@RequestParam("loginName") String loginName);

	/**
	 *  重置密码
	 * @param id
	 * @param newPassword
	 * @param oldPassword
	 * @return
	 */
	@PostMapping("resetPassword")
	boolean resetPassword(
			@RequestParam("userId") String userId , 
			@RequestParam("newPassword") String newPassword, 
			@RequestParam("oldPassword") String oldPassword);

}
