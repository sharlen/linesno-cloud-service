package com.alinesno.cloud.base.storage.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;

import com.alinesno.cloud.base.storage.feign.dto.StorageFileHistoryDto;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 09:19:40
 */
@FeignClient(name="alinesno-cloud-base-storage" , path="storageFileHistory")
public interface StorageFileHistoryFeigin extends IBaseFeign<StorageFileHistoryDto> {

}
