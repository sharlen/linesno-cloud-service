package com.alinesno.cloud.base.message.repository;

import com.alinesno.cloud.base.message.entity.TransactionMessageHistoryEntity;
import com.alinesno.cloud.common.core.orm.repository.IBaseJpaRepository;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 15:19:37
 */
public interface TransactionMessageHistoryRepository extends IBaseJpaRepository<TransactionMessageHistoryEntity, String> {

}
