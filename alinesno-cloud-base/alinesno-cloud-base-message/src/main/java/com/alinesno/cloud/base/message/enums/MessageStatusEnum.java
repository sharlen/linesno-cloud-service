package com.alinesno.cloud.base.message.enums;

/**
 * 消息状态
 * 
 * @author LuoAnDong
 *
 */
public enum MessageStatusEnum {
	
	/**
	 * 预发送(prepared_send):先发送消息至中间库
	 */
	PREPARED("prepared"),
	
	/**
	 * 可发送(enable_send):业务完成确认未发送
	 */
	ENABLE("enable") ,
	
	/**
	 * 发送中(doing_send):业务完成确认并发送出去
	 */
	SENDING("sending"),
	
	/**
	 * 已发送(has_send):消息已经发送并完成业务
	 */
	SENDED("sended") , 
	
	/**
	 * 数据超时  
	 */
	OUTTIME("outtime") ; 
	
	private String value;

	private MessageStatusEnum(String v){
		value = v ; 
	}

	public String value() {
		return value;
	}
}
