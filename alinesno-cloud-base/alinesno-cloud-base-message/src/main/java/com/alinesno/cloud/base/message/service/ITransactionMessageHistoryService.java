package com.alinesno.cloud.base.message.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.message.entity.TransactionMessageHistoryEntity;
import com.alinesno.cloud.base.message.repository.TransactionMessageHistoryRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 15:19:37
 */
@NoRepositoryBean
public interface ITransactionMessageHistoryService extends IBaseService<TransactionMessageHistoryRepository, TransactionMessageHistoryEntity, String> {

}
