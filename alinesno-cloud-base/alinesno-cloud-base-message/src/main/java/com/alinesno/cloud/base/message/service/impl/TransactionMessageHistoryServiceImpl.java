package com.alinesno.cloud.base.message.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.message.entity.TransactionMessageHistoryEntity;
import com.alinesno.cloud.base.message.repository.TransactionMessageHistoryRepository;
import com.alinesno.cloud.base.message.service.ITransactionMessageHistoryService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 15:19:37
 */
@Service
public class TransactionMessageHistoryServiceImpl extends IBaseServiceImpl<TransactionMessageHistoryRepository, TransactionMessageHistoryEntity, String> implements ITransactionMessageHistoryService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(TransactionMessageHistoryServiceImpl.class);

}
