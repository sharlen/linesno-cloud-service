package com.alinesno.cloud.base.print.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2019-06-07 21:26:00
 */
@SuppressWarnings("serial")
public class TemplateVersionDto extends BaseDto {

    /**
     * 打印模板版本号
     */
	private String templateVersion;
	
    /**
     * 打印模板内容
     */
	private String printTemplate;
	
    /**
     * 编辑作者
     */
	private String author;
	
//	private String printTemplate;
//	private String templateVersion;
	


//	public String getTemplateVersion() {
//		return templateVersion;
//	}
//
//	public void setTemplateVersion(String templateVersion) {
//		this.templateVersion = templateVersion;
//	}
//
//	public String getPrintTemplate() {
//		return printTemplate;
//	}
//
//	public void setPrintTemplate(String printTemplate) {
//		this.printTemplate = printTemplate;
//	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPrintTemplate() {
		return printTemplate;
	}

	public void setPrintTemplate(String printTemplate) {
		this.printTemplate = printTemplate;
	}

	public String getTemplateVersion() {
		return templateVersion;
	}

	public void setTemplateVersion(String templateVersion) {
		this.templateVersion = templateVersion;
	}

}
