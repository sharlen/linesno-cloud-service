package com.alinesno.cloud.base.boot.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.base.boot.enums.ResourceTypeEnmus;
import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Entity
@Table(name="content_post_type")
public class ContentPostTypeEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 类型名称
     */
	@Column(name="type_name")
	private String typeName;
    /**
     * 添加时间 
     */
	@Column(name="type_add_time")
	private Date typeAddTime;
    /**
     * 类型状态
     */
	@Column(name="type_status")
	private Integer typeStatus;

	/**
	 * 父类主键
	 */
	private String pid = ResourceTypeEnmus.PLATFORM_RESOURCE_PARENT.value ; 
	
	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Date getTypeAddTime() {
		return typeAddTime;
	}

	public void setTypeAddTime(Date typeAddTime) {
		this.typeAddTime = typeAddTime;
	}

	public Integer getTypeStatus() {
		return typeStatus;
	}

	public void setTypeStatus(Integer typeStatus) {
		this.typeStatus = typeStatus;
	}


	@Override
	public String toString() {
		return "ContentPostTypeEntity{" +
			"typeName=" + typeName +
			", typeAddTime=" + typeAddTime +
			", typeStatus=" + typeStatus +
			"}";
	}
}
