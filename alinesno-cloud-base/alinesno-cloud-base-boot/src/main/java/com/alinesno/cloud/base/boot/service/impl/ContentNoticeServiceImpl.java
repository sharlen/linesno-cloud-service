package com.alinesno.cloud.base.boot.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.boot.entity.ContentNoticeEntity;
import com.alinesno.cloud.base.boot.repository.ContentNoticeRepository;
import com.alinesno.cloud.base.boot.service.IContentNoticeService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-04 14:20:07
 */
@Service
public class ContentNoticeServiceImpl extends IBaseServiceImpl<ContentNoticeRepository, ContentNoticeEntity, String> implements IContentNoticeService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ContentNoticeServiceImpl.class);

}
