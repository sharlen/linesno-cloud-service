package com.alinesno.cloud.base.boot.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.boot.entity.InfoCodeEntity;
import com.alinesno.cloud.base.boot.repository.InfoCodeRepository;
import com.alinesno.cloud.base.boot.service.IInfoCodeService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Service
public class InfoCodeServiceImpl extends IBaseServiceImpl<InfoCodeRepository, InfoCodeEntity, String> implements IInfoCodeService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(InfoCodeServiceImpl.class);

}
