package com.alinesno.cloud.base.boot.repository;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.base.boot.entity.ManagerResourceEntity;
import com.alinesno.cloud.common.core.junit.JUnitBase;

public class ManagerResourceRepositoryTest extends JUnitBase {

	@Autowired
	private ManagerResourceRepository managerResourceRepository ; 
	
	@Test
	public void testFindAllByResourceParent() {
	}

	@Test
	public void testFindByResourceParentAndApplicationId() {
	}

	@Test
	public void testFindAllByApplicationAndAccount() {
	
		// select t2.* from manager_role_resource t1 left join manager_resource t2 on t1.resource_id=t2.id where t1.role_id in(select role_id from manager_account_role t3 where t3.account_id='567263477580693504') and t2.application_id='566970396281143296' ; 
		
		String applicationId = "566970396281143296" ; 
		String accountId = "567263477580693504" ; 
		
		List<ManagerResourceEntity> subResource = managerResourceRepository.findAllByApplicationAndAccount(applicationId, accountId) ; 
		for(ManagerResourceEntity r : subResource) {
			log.debug("r:{}" , ToStringBuilder.reflectionToString(r));
		}
		
	}

}
