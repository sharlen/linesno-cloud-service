package com.alinesno.cloud.compoment.code.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.compoment.code.entity.ProjectRepositoryAccountEntity;
import com.alinesno.cloud.compoment.code.repository.ProjectRepositoryAccountRepository;

/**
 * <p> 版本控制管理 服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@NoRepositoryBean
public interface IProjectRepositoryAccountService extends IBaseService<ProjectRepositoryAccountRepository, ProjectRepositoryAccountEntity, String> {

}
