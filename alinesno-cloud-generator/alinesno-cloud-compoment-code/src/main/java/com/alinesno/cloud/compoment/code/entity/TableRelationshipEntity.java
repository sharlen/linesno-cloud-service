package com.alinesno.cloud.compoment.code.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 模型关系
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Entity
@Table(name="table_relationship")
public class TableRelationshipEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 关系编码
     */
	private String code;
    /**
     * 映射编码
     */
	private String mapClassTableCode;
    /**
     * 是否一对一 Y N
     */
	private String isOneToOne;
    /**
     * 是否一对多Y N
     */
	private String isOneToMany;


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMapClassTableCode() {
		return mapClassTableCode;
	}

	public void setMapClassTableCode(String mapClassTableCode) {
		this.mapClassTableCode = mapClassTableCode;
	}

	public String getIsOneToOne() {
		return isOneToOne;
	}

	public void setIsOneToOne(String isOneToOne) {
		this.isOneToOne = isOneToOne;
	}

	public String getIsOneToMany() {
		return isOneToMany;
	}

	public void setIsOneToMany(String isOneToMany) {
		this.isOneToMany = isOneToMany;
	}


	@Override
	public String toString() {
		return "TableRelationshipEntity{" +
			"code=" + code +
			", mapClassTableCode=" + mapClassTableCode +
			", isOneToOne=" + isOneToOne +
			", isOneToMany=" + isOneToMany +
			"}";
	}
}
