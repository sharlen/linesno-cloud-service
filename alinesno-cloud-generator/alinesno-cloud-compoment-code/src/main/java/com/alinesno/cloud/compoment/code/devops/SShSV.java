/*
 * Powered By [lixin]
 * 代码脚手架工具生成 [rapid-framework]
 */

package com.alinesno.cloud.compoment.code.devops;

import java.io.IOException;

import com.alinesno.cloud.compoment.code.devops.tools.SSh;
import com.alinesno.cloud.compoment.code.devops.tools.WSTools;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;


/**
 *
 */
public interface SShSV {

    boolean close(String key);

    boolean close(String key, WSTools wsTools);

    void sftpUpload(String codes, String fileName, String path, SSh sSh) throws JSchException, SftpException;

    void shell(SSh sSh, String cmd, String key, WSTools wsTools) throws JSchException, IOException;

}
