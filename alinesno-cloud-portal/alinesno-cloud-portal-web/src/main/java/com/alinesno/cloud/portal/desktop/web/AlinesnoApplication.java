package com.alinesno.cloud.portal.desktop.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing ;

import com.alinesno.cloud.common.web.enable.EnableAlinesnoCommonLogin;

/**
 * 启动入口<br/>
 * @EnableSwagger2 //开启swagger2 
 * 
 * @author LuoAnDong 
 * @sine 2019-05-18 14:05:75
 */
@EnableZuulProxy
@EnableJpaAuditing // jpa注解支持
@EnableEurekaClient  // 开启eureka
@SpringBootApplication

@EnableAlinesnoCommonLogin
public class AlinesnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlinesnoApplication.class, args);
	}

}
