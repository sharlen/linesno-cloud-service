package com.alinesno.cloud.demo.activiti.workflow;

/**
 * ShiroKit. (Singleton, ThreadSafe)
 *
 * @author dafei
 */
public class ShiroKit {

	/**
	 * 登录成功时所用的页面。
	 */
	private static String successUrl = "/";

	/**
	 * 登录成功时所用的页面。
	 */
	private static String loginUrl = "/login.html";


	/**
	 * 登录成功时所用的页面。
	 */
	private static String unauthorizedUrl ="/401.jsp";


	/**
	 * Session中保存的请求的Key值
	 */
	private static String SAVED_REQUEST_KEY = "jfinalShiroSavedRequest";
 
	/**
	 * 禁止初始化
	 */
	public ShiroKit() {}
 
	public static final String getSuccessUrl() {
		return successUrl;
	}

	public static final void setSuccessUrl(String successUrl) {
		ShiroKit.successUrl = successUrl;
	}

	public static final String getLoginUrl() {
		return loginUrl;
	}

	public static final void setLoginUrl(String loginUrl) {
		ShiroKit.loginUrl = loginUrl;
	}

	public static final String getUnauthorizedUrl() {
		return unauthorizedUrl;
	}

	public static final void setUnauthorizedUrl(String unauthorizedUrl) {
		ShiroKit.unauthorizedUrl = unauthorizedUrl;
	}
	/**
	 * Session中保存的请求的Key值
	 * @return
	 */
	public static final String getSavedRequestKey(){
		return SAVED_REQUEST_KEY;
	}
 
	/***
	 * 获取登录用户主键
	 * @return
	 */
	public static String getUserId(){
//		SimpleUser user = ShiroKit.getLoginUser();
//		return user.getId();
		
		return null ; 
	}
	/***
	 * 获取登录用户id
	 * @return
	 */
	public static String getUsername(){
//		SimpleUser user = ShiroKit.getLoginUser();
//		return user.getUsername();
		
		return null ; 
	}
	
	/***
	 * 获取用户名称
	 * @return
	 */
	public static String getUserName(){
//		SimpleUser user = ShiroKit.getLoginUser();
//		return user.getName();
		
		return null ; 
	}
	
	 
}