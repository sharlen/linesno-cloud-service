package com.alinesno.cloud.demo.activiti;

import org.activiti.spring.boot.SecurityAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 启动入口<br/>
 * http://localhost:8080/activiti/create  <br/>
 * 用户名/密码：root/admin
 * 
 * @author LuoAnDong
 * @since 2018年8月7日 上午8:45:02
 */
@EnableAsync // 开启异步任务
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
@EnableTransactionManagement //启用事务
public class AlinesnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlinesnoApplication.class, args);
	}

}
