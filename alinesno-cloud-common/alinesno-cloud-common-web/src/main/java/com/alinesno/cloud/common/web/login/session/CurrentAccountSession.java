package com.alinesno.cloud.common.web.login.session;

import javax.servlet.http.HttpServletRequest;

import com.alinesno.cloud.base.boot.feign.dto.ManagerAccountDto;
import com.alinesno.cloud.common.web.login.constants.LoginConstants;

/**
 * 获取当前用户
 * @author LuoAnDong
 * @sine 2019年4月5日 下午4:18:25
 */
public class CurrentAccountSession {

	/**
	 * 获取当前用户
	 * @return
	 */
	public static ManagerAccountDto get(HttpServletRequest request) {
		
		Object o = request.getSession().getAttribute(LoginConstants.CURRENT_USER) ; 
		if(o != null) {
			return (ManagerAccountDto) o ; 
		}
		return null ; 
	}
	
}
