package com.alinesno.cloud.common.core.services;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;
import com.alinesno.cloud.common.core.orm.repository.IBaseJpaRepository;

/**
 * 业务服务基类
 * @author LuoAnDong
 * @since 2018年11月20日 下午7:51:58
 */
@NoRepositoryBean
public interface IBaseService <Jpa extends IBaseJpaRepository<Entity, ID> , Entity extends BaseEntity , ID> extends IBaseJpaRepository<Entity , ID>{

	/**
	 * 批量删除 
	 * @param ids
	 */
	void deleteByIds(ID[] ids);

	boolean modifyHasStatus(ID id);
	
}
